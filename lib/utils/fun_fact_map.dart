class FunFactMap {
  final String text;
  final String permalink;
  final String sourceUrl;
  final String language;
  final String source;

  FunFactMap({
    this.text,
    this.permalink,
    this.sourceUrl,
    this.language,
    this.source,
  });

  factory FunFactMap.fromJson(Map<String, dynamic> json) {
    return FunFactMap(
      text: json['text'],
      permalink: json['permalink'],
      language: json['language'],
      source: json['source'],
      sourceUrl: json['source_url'],
    );
  }
}
