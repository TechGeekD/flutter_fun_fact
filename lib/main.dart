import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fun_fact/utils/fun_fact_map.dart';
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Fun Fact',
      home: FunFactApp(),
    );
  }
}

class FunFactApp extends StatefulWidget {
  @override
  _FunFactAppState createState() => _FunFactAppState();
}

class _FunFactAppState extends State<FunFactApp>
    with SingleTickerProviderStateMixin {
  Animation animation;
  AnimationController animationController;
  String dispFact = 'Loading...';
  Color dispColor = Colors.blue;
  bool isLoadingData = false;
  final Random _random = Random();

  @override
  void initState() {
    super.initState();
    showFact();
    animationController =
        AnimationController(duration: Duration(milliseconds: 800), vsync: this);
    animation = CurvedAnimation(
        parent: animationController, curve: Curves.fastOutSlowIn)
      ..addListener(() {
        setState(() {});
      });

    animationController.forward();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  void showFact() {
    setState(() {
      isLoadingData = true;
    });
    _getData().then((FunFactMap response) {
      FunFactMap funFact = response;
      setState(() {
        isLoadingData = false;
        dispFact = funFact.text;
        dispColor = Color.fromARGB(
          _random.nextInt(256),
          _random.nextInt(256),
          _random.nextInt(256),
          _random.nextInt(256),
        );
        animationController.reset();
        animationController.forward();
      });
    }).catchError((err) {
      setState(() {
        isLoadingData = false;
      });
      print('****************** $err *******************');
    });
  }

  Future<FunFactMap> _getData() async {
    Uri uri = Uri.https(
        'randomuselessfact.appspot.com', '/random.json', {'language': 'en'});
    final http.Response response = await http.get(
      uri,
      headers: {
        'Accept': 'application/json',
      },
    );
    print(json.decode(response.body));
    return FunFactMap.fromJson(json.decode(response.body));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: dispColor,
      body: InkWell(
        onTap: showFact,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 75.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: EdgeInsets.only(left: 30.0),
                    child: Text(
                      'Did You Know !',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 30.0,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    padding:
                        EdgeInsets.only(left: 20.0, right: 20.0, top: 60.0),
                    child: Opacity(
                      opacity: animation.value,
                      child: Transform(
                        transform: Matrix4.translationValues(
                          0.0,
                          animation.value * -50.0,
                          0.0,
                        ),
                        child: Text(
                          dispFact,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 25.0,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
